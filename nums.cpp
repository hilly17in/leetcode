class Solution {
public:

int HALF_INT_MIN = -1073741824;

    int divide(int dividend, int divisor) {
        int quotient = 0;
        int powerofTwo = 0;
        long value = 0 ;
               
        // Special cases: overflow.
        if (dividend == INT_MIN && divisor == -1) {
            return INT_MAX;
        }
        
        if(divisor == -1)
            return -dividend;
        
        if (divisor == 1) {
            return dividend;
        }
        
        int negatives = 2;
        if (dividend > 0) {
            negatives--;
            dividend = -dividend;
        }
        if (divisor > 0) {
            negatives--;
            divisor = -divisor;
        }
        
        value = divisor;
        while(dividend <= value){
            dividend -= value;
            value += value;
            powerofTwo += 1;
        }
        
        if(powerofTwo == 0)
            return 0;
        
        quotient = (2<<(powerofTwo-1))-1;
        
        while(dividend <= divisor){
            dividend -= divisor;
            quotient += 1;
        }
        
        if(negatives==1)
            quotient = -quotient;
        return quotient;
    }
};